extends PathFollow2D

# DON'T WORRY ! The warning is normal, the path to follow is OPTIONAL but godot thinks it is NECESSARY
# Put a path if you want and it will follow it, else it will keep the same behavior than previously

export var SPEED_PX_S = 128
export var active = false

# You can modify loop , automatic rotation, and interpolation directly from the editor

func _ready():
	pass
	
func set_active(b_active):
	if b_active :
		set_fixed_process(true)
		active = true 
	else :
		set_fixed_process(false)
		active = false
		
func _fixed_process(delta):
	set_offset( get_offset() + delta * SPEED_PX_S)
