extends Node2D

const MAX_LOADING_TIME = 10 # seconds

signal loading_finished

var loading = preload("res://scenes/loading/loading.tscn")
var loader = null
var resource_loaded = null

func _ready():
	set_pause_mode(PAUSE_MODE_PROCESS)
	pass

func is_loading():
	return loader != null
			
func _process(delta):
    # poll your loader
	assert(loader)
	var status = loader.poll()

	if status == ERR_FILE_EOF: # load finished
		get_node("loading/bar").set_max( loader.get_stage_count() )
		get_node("loading/bar").set_val( loader.get_stage() )
		resource_loaded = loader.get_resource()
		get_node("loading").queue_free()
		loader = null
		emit_signal("loading_finished", resource_loaded)
		print("[Loader]:\tInteractive load succeeded !")
		
	elif status == OK:
		get_node("loading/bar").set_max( loader.get_stage_count() )
		get_node("loading/bar").set_val( loader.get_stage() )
	else: # error
		loader = null
		get_node("loading").queue_free()
		print("[Loader] ERROR:\tInteractive load failed !")
		yield()


func load_interactive( path ):
	set_process(true)
	connect("loading_finished", self, "_on_loading_finished")
	loader = ResourceLoader.load_interactive(path)
	add_child(loading.instance())
	
func _on_loading_finished(loaded):
	set_process(false)
	disconnect("loading_finished", self, "_on_loading_finished")
	