extends Node2D

var current_scene 

func _ready():
	current_scene = "splashscreen"
	set_pause_mode(PAUSE_MODE_PROCESS)
	
	pass

func get_current_scene():
	return current_scene
	
func goto_scene( name ) :
	current_scene = name
	g_loader.load_interactive("res://scenes/"+name+"/"+name+".tscn")
	g_loader.connect("loading_finished", self, "_on_loading_finished")
	
func _on_loading_finished(scene):
	g_loader.disconnect("loading_finished", self, "_on_loading_finished")
	get_tree().get_current_scene().queue_free()
	var scene_inst = scene.instance()
	get_tree().get_root().add_child(scene_inst)
	get_tree().set_current_scene(scene_inst);