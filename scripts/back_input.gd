 extends Node2D

var pause_scn = preload("res://scenes/pause/pause.tscn")
var instance = null
var in_pause_mode = false
var last_pressed = 0.0

func _ready() :
	set_process(true)
	set_pause_mode(PAUSE_MODE_PROCESS)
	pass
	
func _process(delta):
	last_pressed += delta
	if (Input.is_action_pressed("player0_menu") or Input.is_action_pressed("player1_menu")) and last_pressed >= 0.5:
		toggle_pause()
		last_pressed = 0.0
		print(last_pressed)
		
		#g_scene_manager.goto_scene("splashscreen")
		#set_process(false)
	pass
	
func toggle_pause():
	if in_pause_mode == false :
		get_tree().set_pause(true)
#		print("pause_on")
		instance = pause_scn.instance()
		instance.set_name("pause_child")
		get_tree().get_current_scene().add_child(instance)
		in_pause_mode = true
	else :
#		print("pause_off")
		get_tree().get_current_scene().remove_child(get_tree().get_current_scene().get_node("pause_child"))
		get_tree().set_pause(false)
		in_pause_mode = false